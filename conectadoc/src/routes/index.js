import { Route, Switch } from "react-router-dom";
import FormRegister from "../components/FormRegister/index"
import { useState } from "react"
import Home from "../pages/home";
import Dash from "../components/dashboard";
import Prontuario from "../pages/prontuario";

const Routes = () => {

    const [user, setUser] = useState([]) 

    const [patient, setPatient] = useState([]) 



    const [patients, setPatients] = useState([
        { id: 1, name: 'João', sexo: 'Masculino', idade: 20, type: "Retorno", calls: "15-04-2022", today: false, answered: false, prontuario: []},
        { id: 2, name: 'Jose', sexo: 'Masculino', idade: 20, type: "Consulta", calls: "12-04-2022", today: true, answered: false, prontuario: []},
        { id: 3, name: 'Laura', sexo: 'Feminino', idade: 9, type: "Retorno", calls: "15-04-2022", today: false, answered: false, prontuario: []},
        { id: 4, name: 'Fatima', sexo: 'Feminino', idade: 46, type: "Consulta", calls: "12-04-2022", today: true, answered: false, prontuario: []},
        { id: 5, name: 'Axex', sexo: 'Masculino', idade: 12, type: "Retorno", calls: "15-04-2022", today: false, answered: false, prontuario: []}
    ])

    return(
        <Switch>
            <Route exact path="/">
                <Home/>
            </Route>
            <Route path="/login">
                <FormRegister user={user} setUser={setUser}/>
            </Route>
            <Route path="/dashboard">
                <Dash patients={patients} patient={patient} setPatient={setPatient} />
            </Route>
            <Route path="/prontuario">
                <Prontuario user={patient} />
            </Route>
        </Switch>
    )
}

export default Routes;