import styled from "styled-components";


export const Container = styled.body`
    h2{
    text-align: center;
    }
    form{
    width: 85vw;
    margin: 40px auto;
    padding: 10px;
    border-radius: 7px;
    height: 40vh;
    border: 2px solid black;
    }
    div{
    display: flex;
    flex-direction: column;    
    }
    button{
    background-color: white;
    border: 2px solid black;
    height: 4.5vh;
    width: 20vw;
    border-radius: 5px;
    margin: 10px auto;
    font-weight: bold;
    }

    @media(min-width: 768px){
        form{
            width: 600px;
        }
        button{
            width: 150px;
        }
    }

`