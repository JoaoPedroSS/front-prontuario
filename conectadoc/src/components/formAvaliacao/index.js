import {useForm} from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup"
import { TextField } from "@material-ui/core"
import { Container } from "./styles";


const FormAvaliacao = ({avaliacao, setAvaliacao}) => {
    const schema = yup.object().shape({
        problema: yup.string().required("Campo obrigatório"),
        situacao: yup.string().required("Campo obrigatório"),
        observacoes:yup.string().required("Campo obrigatório"),

    })

    const  {
        register, 
        handleSubmit,
        formState: { errors },   
    } = useForm({resolver: yupResolver(schema)})


    const handleForm = (data) => {
        setAvaliacao([...avaliacao,data])
    }
    return (
        <Container>
            <h2>Avaliação</h2>
                <form onSubmit={handleSubmit(handleForm)}>
                    <div>
                    <TextField 
                    label="Problema"
                    size="small"
                    {...register("problema")}
                    error={!!errors.problema}
                    helperText={errors.problema?.message}/>

                    <TextField 
                    label="Situação"
                    size="small"
                    {...register("situacao")}
                    error={!!errors.situacao}
                    helperText={errors.situacao?.message}/>

                    <TextField 
                    label="Observações"
                    size="small"
                    {...register("observacoes")}
                    error={!!errors.observacoes}
                    helperText={errors.observacoes?.message}/>

                    <button type="submit">Enviar</button>
                    </div>

                </form>
        </Container>

    )
}


export default FormAvaliacao;