import styled from "styled-components";


export const Container = styled.body`
    display: flex;
    flex-direction: column;
    height: 100vh;
    background-image: linear-gradient(to right, #00B4DB, #0083B0 , #023e8a);

    h1{
        color: white ;
        text-shadow: black 0.1em 0.1em 0.2em;
        font-family: 'Poppins', sans-serif;
        text-align: center;
        font-weight: bold;
    }

    form{
        width: 85vw;
        margin: 40px auto;
        padding: 10px;
        border-radius: 7px;
        height: 40vh;
        border: 2px solid black;

    }
    div{
        display: flex;
        flex-direction: column;    
    }

    .input{
        margin: 5px;
        background: transparent;
    }
    
    .btn{
        background-color: white;
        border: 2px solid black;
        height: 4.5vh;
        width: 20vw;
        border-radius: 5px;
        margin: 10px auto;
        font-weight: bold;
    }

    .link{
        display: flex;
        justify-content: end;
        color: black;
        width: 45px;
    }

    .link:hover{
        cursor: pointer;
    }

    @media (min-width: 720px) {
        h1{
            color: white ;
            font-size: 5vw;
        }

        form{
            width: 55vw;

        }

        .btn{
            height: 4.5vh;
            width: 13vw;
            font-size: 20px;
        }
    }
`