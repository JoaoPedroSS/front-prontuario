import {useForm} from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup"
import { TextField } from "@material-ui/core"
import { Link } from "react-router-dom";
import { useHistory } from 'react-router-dom';
import { Container } from "./styles";

const FormRegister = ({user, setUser}) => {

    const history = useHistory();


    const schema = yup.object().shape({
        email: yup.string().email("E-mail inválido").required("Campo obrigatório"),
        password: yup.string().required("Campo obrigatório")

    })

    const  {
        register, 
        handleSubmit,
        formState: { errors },   
    } = useForm({resolver: yupResolver(schema)})



    const handleForm = (data) => {
        setUser([...user, data])
        history.push('/dashboard')
    }


    return (
        <Container>
        <h1>Login</h1>
        <form onSubmit={handleSubmit(handleForm)}>

            <div>
                <TextField
                className="input"
                label="E-mail"
                size="small"
                {...register("email")}
                error={!!errors.email}
                helperText={errors.email?.message}
                />

                <TextField
                className="input"
                label="Password"
                size="small"
                {...register("password")}
                error={!!errors.password}
                helperText={errors.password?.message}
                />
            </div>

            <div>
                <button className="btn" type="submit" variant="contained" color="primary">
                    Enviar
                </button>
                <Link className="link" to="/">Voltar</Link>
            </div> 
        </form>
        </Container>


    )
}


export default FormRegister;