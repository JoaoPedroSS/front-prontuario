import styled from "styled-components";



export const Container = styled.body`
  display: flex;
  flex-direction: column;
  background-image: linear-gradient(to right, #00B4DB, #0083B0 , #023e8a);


  .today{
    width: 88vw;
    margin: 20px auto;
    padding: 1rem;
    border-radius: 0.5rem;
    border: 1px solid white;
  }

  .today h2{
    color: white;
    text-align: center;
    font-family: 'Poppins', sans-serif;
  }

  .today div{
    border: 1px solid white;
    padding: 0.5rem;
    margin: 0.5rem;
    border-radius: 0.5rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    }

    .today p{
    margin: 0.4rem;
    color: white;
    font-weight: bold;
    font-family: 'Akshar', sans-serif;

    }

    .today div button{
    background-color: white;
    border: 1px solid white;
    border-radius: 0.3rem;
    font-weight: bold;
    margin: 10px;
    cursor: pointer;
    }

    .today div button:hover{
    background-color: black;
    color: white;
}
    .btn{
    width: 40vw;
    height: 20px;
    background-color: white;
    border: 1px solid white;
    border-radius: 0.3rem;
    font-weight: bold;
    margin: 10px auto;
    cursor: pointer;
    }

    @media (min-width: 768px){
        .today{
        width: 70vw;
        }

        .today p{
        font-size: 1.4rem;
        }

        .today div button{
        width: 150px;
        height: 34px;
        font-size: 1rem;
        }
        .btn{
        width: 200px;
        height: 40px;
        font-size: 1.3rem;
        }
    }

`