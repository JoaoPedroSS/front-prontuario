import { useHistory } from "react-router-dom";
import { Container } from "./styles";


const Dash = ({patients, patient, setPatient}) => {
    
    const history = useHistory()

    const select = (user) => {
        if (patient.length < 1){
            setPatient([...patient, user])
            history.push('/prontuario')
        }
    }
    
    return(
        <Container>
            <div className="today">
                <h2>Atendimento para hoje</h2>
                {patients.map((user,index) => (
                    user.today === true   ? <div key={index}>
                    <p>Nome do paciente: {user.name}</p>
                    <p>Sexo: {user.sexo}</p>
                    <p>Idade: {user.idade}</p>
                    <p>Tipo de atendimento: {user.type}</p>
                    <button onClick={() => select(user)} >Selecionar</button>
                    </div> : ''
                ))}
            </div>

            <div className="today">
                <h2>Atendimento de tipo Retorno</h2>
                {patients.map((user,index) => (
                    user.type === "Retorno" ? <div key={index}>
                    <p>Nome do paciente: {user.name}</p>
                    <p>Sexo: {user.sexo}</p>
                    <p>Idade: {user.idade}</p>
                    <p>Tipo de atendimento: {user.type}</p>
                    <button onClick={() => select(user)} >Selecionar</button>
                    </div> : ''
                ))}
            </div>

            <div className="today">
                <h2>Paciente atendido</h2>
                {patients.map((user,index) => (
                    user.answered === true ? <div key={index}>
                    <p>Nome do paciente: {user.name}</p>
                    <p>Sexo: {user.sexo}</p>
                    <p>Idade: {user.idade}</p>
                    <p>Tipo de atendimento: {user.type}</p>
                    </div> : ''
                ))}
            </div>

            <button className="btn" onClick={() => history.push("/login")}>Voltar</button>
        </Container>
    )
}


export default Dash;