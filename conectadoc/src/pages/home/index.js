import { useHistory } from "react-router-dom";
import {Container, Box, Aside} from "./styles";
import icon3 from "../../img/pasta1.png";


const Home = () => {
    
    const history = useHistory();

    return(
        <Container>
            <Box>
                <p>A melhor Clínica online faça seu teste grátis</p>

                <button onClick={() => history.push('/login')}>Login</button>
            </Box>
            <Aside>
                <img src={icon3} alt="imagem"/>
            </Aside>
        </Container>
    )
}


export default Home;