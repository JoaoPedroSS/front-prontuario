import styled from "styled-components";

export const Container = styled.body`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: space-around;
  background-image: linear-gradient(to right, #00B4DB, #0083B0 , #023e8a);
`


export const Box = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px;
    
    p{
        text-align: center;
        font-weight: bold;
        color: white;
        font-size: 5vw;
        font-family: 'Open Sans', sans-serif;
    }
    
    button{
        margin: auto;
        font-weight: bold;
        font-size: 3.4vw;
        height: 5vh;
        width: 19vw;
        border-radius: 4px;
        border: 0;
    }

    @media (min-width: 1084px) {
        button{
            height: 8vh;
            width: 19vw;  
        }

        p{
            font-size: 4vw;
        }
    }
`

export const Aside = styled.aside`
    img{
        width: 35vw;
        margin-top: 20px;
    }

    @media (min-width: 1084px) {
        img{
            width: 25vw;
            margin-top: 20px;
        }
    }
`
