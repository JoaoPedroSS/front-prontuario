import styled from "styled-components";


export const Container = styled.body`
    display: flex;
    flex-direction: column;
    background-image: linear-gradient(to right, #00B4DB, #0083B0 , #023e8a);
    h1{
        text-align: center;
    }
    .patiente{
        display: flex;
        flex-direction: column;
        width: 75vw;
        align-items: center;
        margin: 0 auto;
        border: 2px solid black;
        border-radius: 0.5rem;
    }

    .patiente p{
        font-weight: bold;
        font-family: 'Roboto Slab', serif;
        font-size: 1.1rem;
    }

    .div{
        margin: 10px auto;
        display: flex;
        justify-content: center;
    }

    .div button{
        background-color: white;
        border: 2px solid black;
        height: 4.5vh;
        width: 20vw;
        border-radius: 5px;
        margin: 10px auto;
        font-weight: bold;
    }
`