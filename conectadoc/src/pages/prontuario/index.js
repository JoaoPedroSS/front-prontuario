import  { useState } from "react"
import { useHistory } from 'react-router-dom';
import FormSubjetiva from "../../components/formSubjetiva";
import FormObjetiva from "../../components/formObjetiva";
import FormAvaliacao from "../../components/formAvaliacao";
import FormPlano from "../../components/formPlano";
import  {Container} from "./styles"

const Prontuario = ({user}) => {
    const [subjetiva, setSubjetiva ] = useState([])

    const [objetiva, setObjetiva ] = useState([])

    const [avaliacao, setAvaliacao ] = useState([])

    const [plano, setPlano ] = useState([])


    const history = useHistory();

    const nullPatient = () => {
        user.shift();
        history.push("/dashboard")
    }

    return (
        <Container>
            <h1>Prontuario</h1>
            {user.map((patient,index) => (
                    <div className="patiente" key={index}>
                    <p>Nome do paciente: {patient.name}</p>
                    <p>Sexo: {patient.sexo}</p>
                    <p>Idade: {patient.idade}</p>
                    <p>Tipo de atendimento: {patient.type}</p>
                    {patient.answered = true}
                    </div>
            ))}

            <FormSubjetiva subjetiva={subjetiva} setSubjetiva={setSubjetiva}/>
            <FormObjetiva objetiva={objetiva} setObjetiva={setObjetiva}/>
            <FormAvaliacao avaliacao={avaliacao} setAvaliacao={setAvaliacao} />
            <FormPlano plano={plano} setPlano={setPlano}/>
        

            <div className="div">
                <button onClick={nullPatient}>Voltar</button>
            </div>
        </Container>
    )
}


export default Prontuario;